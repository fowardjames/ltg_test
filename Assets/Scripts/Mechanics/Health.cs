using System;
using Platformer.Gameplay;
using UnityEngine;
using static Platformer.Core.Simulation;

namespace Platformer.Mechanics
{
    public class Health : MonoBehaviour
    {
        public int maxHP = 1;
        public bool IsAlive => CurrentHP > 0;

		public int CurrentHP { get; private set; }
		public int CurrentLives { get => currentLives; set => currentLives = value; }

		int currentHP;

        [SerializeField] private int currentLives = 3; 

        void Awake()
        {
            CurrentHP = maxHP;
        }

        public void Increment()
        {
            CurrentHP = Mathf.Clamp(CurrentHP + 1, 0, maxHP);
        }
        public void Decrement()
        {
            CurrentHP = Mathf.Clamp(CurrentHP - 1, 0, maxHP);
            if (CurrentHP == 0)
            {
                var ev = Schedule<HealthIsZero>();
                ev.health = this;
            }
        }

        public void Die()
        {
            while (CurrentHP > 0) Decrement();

            CurrentLives--; 
        }
    }
}
