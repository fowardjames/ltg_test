﻿using System.Collections;
using System.Collections.Generic;
using Platformer.Gameplay;
using UnityEngine;
using static Platformer.Core.Simulation;

namespace Platformer.Mechanics
{
    /// <summary>
    /// DeathZone components mark a collider which will schedule a
    /// PlayerEnteredDeathZone event when the player enters the trigger.
    /// </summary>
    public class DeathZone : MonoBehaviour
    {
        void OnTriggerEnter2D(Collider2D collider)
        {
            var playerController = collider.gameObject.GetComponent<PlayerController>();
            if (playerController != null)
            {
                var enteredDeathZone = Schedule<PlayerEnteredDeathZone>();
                enteredDeathZone.deathzone = this;
            }
        }
    }
}