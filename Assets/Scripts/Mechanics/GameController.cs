using Platformer.Core;
using Platformer.Model;
using UnityEngine;
using System.Collections.Generic;

namespace Platformer.Mechanics
{
    /// <summary>
    /// This class exposes the the game model in the inspector, and ticks the
    /// simulation.
    /// </summary> 
    public class GameController : MonoBehaviour
    {
        static List<float> _tickTimes = new List<float>();

        public static GameController Instance { get; private set; }

        public PlatformerModel model = Simulation.GetModel<PlatformerModel>();

        void OnEnable()
        {
            Instance = this;
        }

        void Update()
        {
            if (Instance == this)
            {
                Simulation.Tick();
                _tickTimes.Add(Time.deltaTime);
            }
        }

        void OnDisable()
        {
            if (Instance == this) Instance = null;
        }

     
    }
}