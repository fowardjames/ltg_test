using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Platformer.Mechanics
{
    public class EntityTraitController : MonoBehaviour
    {
        string ReturnNameRandomName()
        {
            string[] possibleNames = new string[] { "Albert", "Robert", "James", "Harry", "David" };
            int x = Random.Range(1, 9999);

            string enemyName = possibleNames[Random.Range(0, possibleNames.Length)] + x;
            return enemyName;
        }

        string ReturnLikes()
        {
            string[] possibleLikes = new string[] { "Apples", "Cheese", "Malmite", "Bacon", "Milk", "Carrots", "Music" };

            string selected = possibleLikes[Random.Range(0, possibleLikes.Length)];
            return selected;
        }

        string makedebug()
        {
            string debug;

            debug = "";
            debug += GameConstants.EnemyTypeName;
            debug += ": ";
            debug += GameConstants.EnemyNameDeclaration;
            debug += " ";
            debug += ReturnNameRandomName();
            debug += " ";
            debug += GameConstants.EnemyLikeDeclaration;
            debug += " ";
            debug += ReturnLikes();
            debug += " ";

            return debug;
        }

        IEnumerator debugit()
        {
            yield return new WaitForSeconds(Random.Range(2, 10));
            Debug.Log(makedebug());
            StartCoroutine(debugit());
        }
    }
}
