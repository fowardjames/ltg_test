using Platformer.Gameplay;
using System.Collections;
using UnityEngine;
using static Platformer.Core.Simulation;

using Models;
 
namespace Platformer.Mechanics
{
    /// <summary>
    /// This class contains the data required for implementing token collection mechanics.
    /// It does not perform animation of the token, this is handled in a batch by the 
    /// TokenController in the scene.
    /// </summary>
    [RequireComponent(typeof(Collider2D))]
    public class TokenInstance : MonoBehaviour
    {
        public AudioClip tokenCollectAudio;

        internal SpriteRenderer _renderer;

        //unique index which is assigned by the TokenController in a scene.
        internal int tokenIndex = -1;
        //active frame in animation, updated by the controller.
        internal int frame = 0;

        // Token model
        internal TokenModel tokenModel = new TokenModel();

        private Animator _animator; 

        void Awake()
        {
            _renderer = GetComponent<SpriteRenderer>();
            _animator = GetComponent<Animator>(); 
        }
		private void Start()
		{
            StartCoroutine(WaitToTurnOn());
		}

        private IEnumerator WaitToTurnOn()
        {
            yield return new WaitForSeconds(Random.Range(0.0f, 1.0f));
            _animator.enabled = true; 
        }

		void OnTriggerEnter2D(Collider2D other)
        {
			//only exectue OnPlayerEnter if the player collides with this token.
			if (other.gameObject.CompareTag("Player"))
			{
                var player = other.gameObject.GetComponent<PlayerController>();
                if (player != null) OnPlayerEnter(player);
            }
           
        }

        void OnPlayerEnter(PlayerController player)
        {
            if (tokenModel.collected) return;
            //disable the gameObject and remove it from the controller update list.
            frame = 0;
            //send an event into the gameplay system to perform some behaviour.
            var ev = Schedule<PlayerTokenCollision>();
            ev.token = this;
            ev.player = player;
            _animator.SetTrigger("Collect");
        }

        public void DestroyGameObject()
        {
            Destroy(this.gameObject);
        }
     

    }
}