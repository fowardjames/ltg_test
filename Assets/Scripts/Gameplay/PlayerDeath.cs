﻿using System.Collections;
using System.Collections.Generic;
using Platformer.Core;
using Platformer.Model;
using Platformer.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Platformer.Gameplay
{
    /// <summary>
    /// Fired when the player has died.
    /// </summary>
    /// <typeparam name="PlayerDeath"></typeparam>
    public class PlayerDeath : Simulation.Event<PlayerDeath>
    {
        PlatformerModel model = Simulation.GetModel<PlatformerModel>();

        public override void Execute()
        {
            var player = model.player;
            if (player.health.IsAlive)
            {
                player.health.Die();
                model.virtualCamera.m_Follow = null;
                model.virtualCamera.m_LookAt = null;
                player.collider2d.enabled = false;
                player.controlEnabled = false;
                player.rigidbody2D.simulated = false;
                if (player.audioSource && player.ouchAudio)
                    player.audioSource.PlayOneShot(player.ouchAudio);
                player.animator.SetTrigger("hurt");
                player.animator.SetBool("dead", true);

                Simulation.Schedule<UpdateLivesUI>();
                Simulation.Schedule<PlayerSpawn>(2);
				if (model.player.health.CurrentLives <= 0)
				{
                    //If I had more time I would of implemented a cleaner restart if the scene
                    PlayerPrefs.SetInt("CurrentToken", 0);
                    SceneManager.LoadScene(0);
				}
            }
        }
    }
}