using UnityEngine;

namespace Models
{
    [System.Serializable]
    public class TokenModel
    { 
        public bool collected = false;
    }
}