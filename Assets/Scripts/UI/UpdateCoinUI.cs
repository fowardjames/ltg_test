using Platformer.Core;
using Platformer.Model;
using Platformer.UI;

namespace Platformer.Gameplay
{
    /// <summary>
    /// Fired when the player has died.
    /// </summary>
    /// <typeparam name="PlayerDeath"></typeparam>
    public class UpdateCoinUI : Simulation.Event<UpdateCoinUI>
    {
        PlatformerModel model = Simulation.GetModel<PlatformerModel>();
 
        public override void Execute()
        {
            model.uiController.PopulateCoin(); 
        }
    }
}