﻿using Platformer.Core;
using Platformer.Model;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Platformer.UI
{
    /// <summary>
    /// A simple controller for switching between UI panels.
    /// </summary>
    public class MainUIController : MonoBehaviour
    {
        public GameObject[] panels;

        [SerializeField] private GameObject _heartPrefab;
        [SerializeField] private Transform _heartParent;
        [SerializeField] private TextMeshProUGUI _coinText; 

        PlatformerModel model = Simulation.GetModel<PlatformerModel>();


		private void Start()
		{
            PopulateCurrentLives();
            PopulateCoin(); 

        }

        public void SetActivePanel(int index)
        {
            for (var i = 0; i < panels.Length; i++)
            {
                var active = i == index;
                var g = panels[i];
                if (g.activeSelf != active) g.SetActive(active);
            }
        }

        public void PopulateCurrentLives()
        {
            foreach (Transform child in _heartParent)
            {
                GameObject.Destroy(child.gameObject);
            }
			for (int i = 0; i < model.player.health.CurrentLives; i++)
			{
                Instantiate(_heartPrefab, _heartParent);
			}
        }

        public void PopulateCoin()
        {
            _coinText.text = PlayerPrefs.GetInt("CurrentToken").ToString(); 
        }

    }
}